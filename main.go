package main

//Практическое задание
//Написать библиотеку, которая предоставляет ряд методов
//получение количества строк в текстовом файле
//получение количества слов в текстовом файле
//В методах необходимо предусмотреть возврат ошибок, которые могут произойти при чтении файла.
//
//Для того, чтобы использовать эту библиотеку, в GitLab необходимо будет создать тэг.
//
//Написать приложение, которое использует эту библиотеку
//Если при запуске передан флаг line, то приложение должно выводить на экран количество строк в текстовом файле,
//если флаг word, то выводить на экран количество слов в текстовом файле. Оба флага могут быть указаны одновременно.
//
//Название директории и название файла должны читаться основным приложением из конфигурационного yaml-файла.
//Сам файл конфигураций должен задаваться ключем -c. Если ключ не указан, то название конфигурационного файла должно читаться из переменных сред.
//
//При выполнении задания необходимо использовать go-модули.
import (
	"errors"
	"flag"
	counter "gitlab.com/ammukovo/ammukovo-module3-lib"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"os"
)

// Ссылка на MR c либой: https://gitlab.com/ammukovo/ammukovo-module3-lib/-/merge_requests/1

type config struct {
	Filename string `yaml:"filename"`
}

func main() {
	countLines := flag.Bool("line", false, "Count lines")
	countWords := flag.Bool("word", false, "Count words")
	var configFilepath string
	flag.StringVar(&configFilepath, "c", "", "Filename")
	flag.Parse()

	if configFilepath == "" {
		configFilepath = os.Getenv("AMMUKOVO_CONFIG_FILEPATH")
		if configFilepath == "" {
			err := errors.New("no config filename presented")
			log.Fatal(err)
		}
	}

	config := readConfig(configFilepath)
	filename := config.Filename

	if *countLines {
		lines, err := counter.CountLines(filename)
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("Lines: %v\n", lines)
	}

	if *countWords {
		words, err := counter.CountWords(filename)
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("Words: %v\n", words)
	}
}

func readConfig(filename string) config {
	var cnf config
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatal(err)
	}
	err = yaml.Unmarshal(data, &cnf)
	if err != nil {
		log.Fatal(err)
	}
	return cnf
}
