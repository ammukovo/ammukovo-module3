module gitlab.com/ammukovo/ammukovo-module3

go 1.18

require (
	gitlab.com/ammukovo/ammukovo-module3-lib v0.0.2
	gopkg.in/yaml.v2 v2.4.0
)
